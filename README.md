# La Guerre des Moutons

#### Gaël Jean François RANDRIA & Ali-Shan KASSOU MAMODE (*TG5 Spé NSI*)

Ce projet consistera à modéliser le fonctionnement d'un écosystème avec :
- De l'herbe et des moutons
- Des loups
- Des chasseurs

Pendant la simulation, les `moutons` et les `loups` se déplacent. Les `loups` mangent les `moutons` ou meurent, tout comme les `moutons` doivent manger de l'`herbe` pour survivre. L'`herbe` doit pouvoir pousser. Les `chasseurs` tuent des `loups`...

Voici le noms des class crées pour que le projet puisse fonctionner :
* Class Monde
* Class Moutons
* Class Loup
* Class Chasseur
* Class Simulation 

Nous allons réaliser cette simulation à partir d'un éditeur Python en utilisant un paradigme de programmation objet.

Pour coder, nous avons utilisé un éditeur de texte en ligne qui est `replit`. Grâce à cette plateforme, nous avons pu collaborer pour que le projet soit fini à temps.
