# La Guerre des Moutons
# Gaël Jean François RANDRIA
# Ali-Shan KASSOU MAMODE
# TNSI

import matplotlib.pyplot as plt
from random import randint

class Monde :
    
    def __init__(self, dimension, duree_repousse) :
        """
             dimension : Entier, représente la taille du côté
        duree_repousse : Entier, vitesse de repousse de l'herbe
                 carte : Liste de listes, Matrice (dimension x dimension)
        """
        self.dimension = dimension 
        self.duree_repousse = duree_repousse
        self.carte = [[randint(0, 2*duree_repousse) for j in range(dimension)] for i in range(dimension)]
        
    def herbePousse(self) :
        """Fait repousser l'herbe dans les cases de la matrice monde"""
        for i in range(self.dimension) :
            for j in range(self.dimension) :
                self.carte[i][j] += 1
    
    def herbeMangee(self, i, j) :
        """i et j sont les indices du coefficient de la matrice carte"""
        if self.carte[i][j] >= self.duree_repousse :
            self.carte[i][j] = 0
    
    def nbHerbe(self) :
        """Retourne le nombre de cases qui contiennent de l'herbe"""
        compteur = 0
        for i in range(self.dimension) :
            for j in range(self.dimension) :
                if self.carte[i][j] >= self.duree_repousse :
                    compteur += 1
        return compteur
    
    def getCoefCarte(self, i, j) :
        """Accesseur de type particulier qui renvoie la valeur du coefficient a(i,j) de la carte. i et j sont les indices du coefficient de la matrice carte"""
        return self.carte[i][j]
    
########################################################################################################

class Mouton :
    
    def __init__(self, monde) :
        """
                         monde : Objet, monde dans lequel se trouve le mouton
               gain_nourriture : Nombre, gain d'énergie apporté par la consommation d'un carré d'herbe (ici 4)
        position_x, position_y : Couple x,y qui représente la position du mouton
                       energie : Entier positif (ou nul), quand l'énergie d'un mouton est à 0, il meurt et l'objet est supprimé. Initialisé entre 1 et 2*gain_nourriture
             taux_reproduction : Entiers compris entre 1 et 100, ce taux de représente le pourcentage de chance qu'un mouton se reproduise (ici 4%)
        """
        self.monde = monde
        self.gain_nourriture = 4 
        self.position_x = randint(0, monde.dimension - 1)
        self.position_y = randint(0, monde.dimension - 1)
        self.energie = randint(1, 2*4)
        self.taux_reproduction = 4
    
    def variationEnergie(self) :
        """Diminue de 1 l'énergie du mouton s'il n'est pas sur un carré d'herbe, sinon augmente de gain_nourriture. Renvoie energie"""
        if self.monde.getCoefCarte(self.position_x, self.position_y) < self.monde.duree_repousse :
            self.energie -= 1
        else :
            self.energie += self.gain_nourriture
            self.monde.herbeMangee(self.position_x, self.position_y)
        return self.energie
    
    def deplacement(self) :
        """Déplace le mouton sur l'une des 8 cases adjacentes à la sienne"""
        self.position_x = randint(self.position_x - 1, self.position_x + 1) % self.monde.dimension
        self.position_y = randint(self.position_y - 1, self.position_y + 1) % self.monde.dimension
        
    
    def place_mouton(self, i, j) :
        """Place un mouton aux coordonées i,j"""
        if (0 <= i < self.monde.dimension) and (0 <= j < self.monde.dimension) :
            self.position_x, self.position_y = i, j
      
########################################################################################################

class Loup :
    
    def __init__(self, monde, moutons) :
        """
                         monde : Objet, monde dans lequel se trouve le loup
               gain_nourriture : Nombre, gain d'énergie apporté par la consommation d'un mouton (ici 18)
        position_x, position_y : Couple x,y qui représente la position du loup
                       energie : Entier positif (ou nul), quand l'énergie d'un loup est à 0, il meurt et l'objet est supprimé. Initialisé entre 1 et 2*gain_nourriture
             taux_reproduction : Entiers compris entre 1 et 100, ce taux de représente le pourcentage de chance qu'un mouton se reproduise (ici 5%)
                       moutons : Liste d'objets Moutons
        """
        self.monde = monde
        self.gain_nourriture = 18
        self.position_x = randint(0, monde.dimension - 1)
        self.position_y = randint(0, monde.dimension - 1)
        self.energie = randint(1, 2*18)
        self.taux_reproduction = 5
        self.moutons = moutons
    
    def variationEnergie(self) :
        """Diminue de 1 l'énergie du loup s'il n'est pas sur un carré d'herbe, sinon augmente de gain_nourriture. Renvoie energie"""
        sur_mouton = False
        for mouton in self.moutons :
            if (self.position_x, self.position_y) == (mouton.position_x, mouton.position_y) :
                sur_mouton = True
                self.moutons.remove(mouton)
        if not sur_mouton :
            self.energie -= 1
        else :
            self.energie += self.gain_nourriture
        return self.energie
    
    def deplacement(self) :
        """Déplace le loup sur l'une des 8 cases adjacentes à la sienne. Si un mouton se trouve sur une de ces 8 cases, le loup s'y dirigera"""
        mouton_proche = False
        coordonnees_mouton = self.position_x, self.position_y
        for i in range(self.position_x - 1, self.position_y + 2) :
            for j in range(self.position_y - 1, self.position_y + 2) :
                for mouton in self.moutons :
                    if (i, j) == (mouton.position_x, mouton.position_y) :
                        mouton_proche = True
                        coordonnees_mouton = i, j
        if mouton_proche :
            self.position_x, self.position_y = coordonnees_mouton
        else :
            self.position_x = randint(self.position_x - 1, self.position_x + 1) % self.monde.dimension
            self.position_y =  randint(self.position_y - 1, self.position_y + 1) % self.monde.dimension
    
    def place_loup(self, i, j) :
        """Place un loup aux coordonées i,j"""
        if (0 <= i < self.monde.dimension) and (0 <= j < self.monde.dimension) :
            self.position_x, self.position_y = i, j
      
########################################################################################################

class Chasseur :
    
    def __init__(self, monde, loups) :
        """
                         monde : Objet, monde dans lequel se trouve le chasseur
        position_x, position_y : Couple x,y qui représente la position du chasseur
                         loups : Liste d'objets Loups
        """
        self.monde = monde
        self.position_x = randint(0, monde.dimension - 1)
        self.position_y = randint(0, monde.dimension - 1)
        self.loups = loups
    
    def deplacement(self) :
        """Déplace le chasseur sur l'une des 8 cases adjacentes à la sienne. Si un loup se trouve sur une de ces 8 cases, le chasseur s'y dirigera"""
        loup_proche = False
        coordonnees_loup = self.position_x, self.position_y
        for i in range(self.position_x - 1, self.position_y + 2) :
            for j in range(self.position_y - 1, self.position_y + 2) :
                for loup in self.loups :
                    if (i, j) == (loup.position_x, loup.position_y) :
                        loup_proche = True
                        coordonnees_loup = i, j
        if loup_proche :
            self.position_x, self.position_y = coordonnees_loup
        else :
            self.position_x = randint(self.position_x - 1, self.position_x + 1) % self.monde.dimension
            self.position_y =  randint(self.position_y - 1, self.position_y + 1) % self.monde.dimension
    
    def chasse(self) :
        """Élimine un loup s'il se trouve sur la même case que le chasseur"""
        sur_loup = False
        for loup in self.loups :
            if (self.position_x, self.position_y) == (loup.position_x, loup.position_y) :
                sur_loup = True
                chance = randint(0, 1)
                if chance == 1 :
                    self.loups.remove(loup)

    def place_chasseur(self, i, j) :
        """Place un chasseur aux coordonées i,j"""
        if (0 <= i < self.monde.dimension) and (0 <= j < self.monde.dimension) :
            self.position_x, self.position_y = i, j
      
########################################################################################################

class Simulation :
    
    def __init__(self, nombre_moutons, nombre_loups, fin_du_monde, moutons, loups, chasseurs, monde, limite_moutons) :
        """
           nombre_moutons : Entier, nombre de moutons initialement présents sur la carte
             nombre_loups : Entier, nombre de loups initialement présents sur la carte
                  horloge : Entier, initialisé à 0
             fin_du_monde : Entier, temps maximum de la simulation
                  moutons : Liste d'objets Moutons
                    loups : Liste d'objets Loups
                chasseurs : Liste d'objets Chasseurs
                    monde : Instance de la classe Monde
          resultats_herbe : Liste construite au fur et à mesure, donnant le nombre de case contenant de l'herbe
        resultats_moutons : Liste construite au fur et à mesure, donnant le nombre de moutons vivants
          resultats_loups : Liste construite au fur et à mesure, donnant le nombre de loups vivants
           limite_moutons : Entier, nombre maximal de moutons pendant la simulation. Si cette limite est franchie, la simulation s'arrête
        """
        self.nombre_moutons = nombre_moutons
        self.nombre_loups = nombre_loups
        self.horloge = 0 
        self.fin_du_monde = fin_du_monde
        self.moutons = moutons
        self.loups = loups
        self.chasseurs = chasseurs
        self.monde = monde
        self.resultats_herbe = []
        self.resultats_moutons = []
        self.resultats_loups = []
        self.limite_moutons = limite_moutons

    def reproductionMoutons(self) :
        """Gère la reproduction des moutons et met à jour leur nombre"""
        initial = [mouton for mouton in self.moutons]
        for mouton in initial :
            chance = randint(1,100)
            if chance <= mouton.taux_reproduction :
                self.moutons.append(Mouton(self.monde))
        self.nombre_moutons = len(self.moutons)
    
    def reproductionLoups(self) :
        """Gère la reproduction des loups et met à jour leur nombre"""
        initial = [loup for loup in self.loups]
        for loup in initial :
            chance = randint(1,100)
            if chance <= loup.taux_reproduction :
                self.loups.append(Loup(self.monde, self.moutons))
        self.nombre_loups = len(self.loups)
    
    def vieMoutons(self) :
        """Gère la variation de l'énergie de chaque mouton, leur mort et leurs déplacements"""
        for mouton in self.moutons :
            if mouton.variationEnergie() == 0 :
                self.moutons.remove(mouton)
                self.nombre_moutons -= 1
            else :
                mouton.deplacement()
            
    def vieLoups(self) :
        """Gère la variation de l'énergie de chaque loup, leur mort et leurs déplacements"""
        for loup in self.loups :
            if loup.variationEnergie() == 0 :
                self.loups.remove(loup)
                self.nombre_loups -= 1
            else :
                loup.deplacement()
    
    def vieChasseurs(self) :
        """Gère les déplacements de chaque chasseur"""
        for chasseur in self.chasseurs :
            chasseur.chasse()
            chasseur.deplacement()

    def simMouton(self) :
        """Gère la simulation en créant une boucle. Renvoie deux listes (resultats_herbe et resultats_moutons)"""
        while self.horloge < self.fin_du_monde and 0 < self.nombre_moutons < self.limite_moutons :
            self.horloge += 1
            self.monde.herbePousse()
            self.vieMoutons()
            self.reproductionMoutons()
            self.vieLoups()
            self.reproductionLoups()
            self.vieChasseurs()
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.resultats_moutons.append(self.nombre_moutons)
            self.resultats_loups.append(self.nombre_loups)
        temps = [i for i in range(1, self.horloge + 1)]
        plt.plot(temps,self.resultats_herbe, label = 'Herbe')
        plt.plot(temps,self.resultats_moutons, label = 'Moutons')
        plt.plot(temps,self.resultats_loups, label = 'Loups')
        plt.legend()
        plt.show()

########################################################################################################

# Monde
dimension = input("Entrez la taille de la carte : ")
while type(dimension) != int :
    try :
        dimension = int(dimension)
    except :
        print("L'entrée demandée doit être un entier\n")
        dimension = input("Entrez la taille de la carte : ")

duree_repousse = input("Entrez la vitesse de repousse de l'herbe : ")
while type(duree_repousse) != int :
    try :
        duree_repousse = int(duree_repousse)
    except :
        print("L'entrée demandée doit être un entier\n")
        duree_repousse = input("Entrez la vitesse de repousse de l'herbe : ")

limite_moutons = input("Entrez le nombre maximal de moutons pendant la simulation : ")
while type(limite_moutons) != int :
    try :
        limite_moutons = int(limite_moutons)
    except :
        print("L'entrée demandée doit être un entier\n")
        limite_moutons = input("Entrez le nombre maximal de moutons pendant la simulation : ")

monde = Monde(dimension, duree_repousse)

# Moutons
nombre_moutons = input("Entrez le nombre de moutons : ")
while type(nombre_moutons) != int :
    try :
        nombre_moutons = int(nombre_moutons)
    except :
        print("L'entrée demandée doit être un entier\n")
        nombre_moutons = input("Entrez le nombre de moutons : ")
        
moutons = [Mouton(monde) for i in range(nombre_moutons)]

# Loups
nombre_loups = input("Entrez le nombre de loups : ")
while type(nombre_loups) != int :
    try :
        nombre_loups = int(nombre_loups)
    except :
        print("L'entrée demandée doit être un entier\n")
        nombre_loups = input("Entrez le nombre de loups : ")
        
loups = [Loup(monde, moutons) for i in range(nombre_loups)]

# Chasseurs
nombre_chasseurs = dimension // 10
chasseurs = [Chasseur(monde, loups) for i in range(nombre_chasseurs)]

# Simulation
fin_du_monde = input("Entrez le temps maximum de la simulation : ")
while type(fin_du_monde) != int :
    try :
        fin_du_monde = int(fin_du_monde)
    except :
        print("L'entrée demandée doit être un entier\n")
        fin_du_monde = input("Entrez le temps maximum de la simulation : ")

Simu = Simulation(nombre_moutons, nombre_loups, fin_du_monde, moutons, loups, chasseurs, monde, limite_moutons)
Simu.simMouton()
